﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AvivaTestMVC.Models;
using AvivaTestMVC.ViewModel;

namespace AvivaTestMVC.Controllers
{
    public class AvivaController : Controller
    {
        //
        // GET: /Aviva/

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.DoneBy = "Vijay Babu";
            return View();
        }

        [HttpPost]
        public ActionResult Index(IndexViewModel objtestData, string direction)
        {
            if (ModelState.IsValid)
            {
                objtestData.CreatedDate = System.DateTime.Now;

                List<int> objnolist = new List<int>();
                if (direction == "Submit")
                {
                    //  Save number to DB
                    AvivaTest objavi = new AvivaTest();
                    objavi.EnteredNumber = objtestData.EnteredNumber;
                    objavi.CreatedDate = System.DateTime.Now;

                    TestVijayEntities1 objDbContext = new TestVijayEntities1();
                    objDbContext.AvivaTests.Add(objavi);
                    objDbContext.SaveChanges();

                    // Show only first 20 numbers
                    objtestData.PagingStart = 1;
                    if (objtestData.EnteredNumber > 20)
                    {
                        objtestData.PagingEnd = 20;
                        objtestData.Next = true;
                        objtestData.Previous = false;
                    }
                    else
                    {
                        objtestData.PagingEnd = objtestData.EnteredNumber;
                        objtestData.Next = false;
                        objtestData.Previous = false;
                    }
                }

                if (direction == "Next")  // On Next 
                {
                    objtestData.PagingStart = objtestData.PagingEnd + 1;
                    if (objtestData.PagingEnd + 20 < objtestData.EnteredNumber)
                    {
                        objtestData.PagingEnd = objtestData.PagingEnd + 20;
                        objtestData.Next = true;
                        objtestData.Previous = true;
                    }
                    else
                    {
                        objtestData.PagingEnd = objtestData.EnteredNumber;
                        objtestData.Next = false;
                        objtestData.Previous = true;
                    }
                }

                if (direction == "Previous")  //On Previous 
                {
                    objtestData.PagingEnd = objtestData.PagingStart -1 ;
                    if (objtestData.PagingEnd - 20 >0)
                    {
                        objtestData.PagingStart = objtestData.PagingStart - 20;
                        objtestData.Next = true;
                        objtestData.Previous = true;
                    }
                    else
                    {
                        objtestData.PagingStart = 1;
                        objtestData.Next = true;
                        objtestData.Previous = false;
                    }
                }

                for (int i = objtestData.PagingStart; i <= objtestData.PagingEnd; i++)
                {
                    objnolist.Add(i);
                }

                objtestData.NumberList = objnolist;

                ModelState.Clear();
                return View(objtestData);
            }
            else
            {
                ModelState.Clear();
                return View(objtestData);
            }
        }

    }
}
