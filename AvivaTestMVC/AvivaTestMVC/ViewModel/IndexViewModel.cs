﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AvivaTestMVC.ViewModel
{
    public class IndexViewModel
    {
        [Required(ErrorMessage = "*")]
        [Range(1, 1000, ErrorMessage = "Enter number between 1 to 1000")]
        [Display(Name = "Enter a Number")]
        public int EnteredNumber { get; set; }

        public DateTime CreatedDate { get; set; }
        public List<int> NumberList { get; set; }

        public int PagingStart { get; set; }
        public int PagingEnd { get; set; }

        public bool Next { get; set; }
        public bool Previous { get; set; }  
    }
}